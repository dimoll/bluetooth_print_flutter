import 'package:meta/meta.dart';

class PrinterHelper {
  PrinterHelper._() {
    allSymbolsCount = 48;
    firstColumnSymbolsCount = 29;
    secondColumnSymbolsCount = 14;
    spacingSymbolsCount = 5;
  }
  factory PrinterHelper() {
    _instance ??= PrinterHelper._();
    return _instance;
  }

  static PrinterHelper _instance;

  int allSymbolsCount;
  int firstColumnSymbolsCount;
  int secondColumnSymbolsCount;
  int spacingSymbolsCount;

  void setPrintSettings({
    @required int all,
    @required int firstColumnSizePercent,
    @required int secondColumnSizePercent,
  }) {
    allSymbolsCount = all;
    firstColumnSymbolsCount = (all * (firstColumnSizePercent / 100)).toInt();
    secondColumnSymbolsCount = (all * (secondColumnSizePercent / 100)).toInt();
    spacingSymbolsCount =
        all - firstColumnSymbolsCount - secondColumnSymbolsCount;
    print(allSymbolsCount);
    print(firstColumnSymbolsCount);
    print(secondColumnSymbolsCount);
    print(spacingSymbolsCount);
  }

  //TODO: set parameters for printing value
  String getPrintedValue() {
    final List<String> data = <String>[
      'Пицца Маргарита',
      'Это очень очень очень очень очень очень очень очень очень длинное название пиццы',
      'Neapolian пицца',
      'что-то',
    ];
    final List<String> price = <String>[
      '25.00',
      '160.00',
      '1654.00',
      '7.00',
    ];
    final List<String> count = <String>[
      '1 x 25.00',
      '20 x 8.00',
      '1 x 1654.00',
      '7 x 1.00',
    ];
    String result = '';
    for (int i = 0; i < data.length; i++) {
      if (data[i].length > firstColumnSymbolsCount) {
        result += getPriceMultiLine(data[i], price[i]);
      } else {
        result += getPriceLine(data[i], price[i]);
      }
      result += count[i];
      result += '\n';
    }

    return result;
  }

  String getPriceMultiLine(String name, String price) {
    final List<String> result = [];
    int removedIndex = 0;
    final List<String> words = name.split(' ');
    String line = '';
    words.forEach((word) {
      if (line.length + word.length <= firstColumnSymbolsCount) {
        line += word;
        line += ' ';
        removedIndex = words.indexOf(word);
      } else {
        return;
      }
    });
    if (removedIndex != null) {
      if (removedIndex == 0)
        words.removeAt(0);
      else
        words.removeRange(0, removedIndex);
    }
    removedIndex = null;

    line = getPriceLine(line, price);
    result.add(line);
    line = '';
    while (words.isNotEmpty) {
      bool isInProcess = true;
      for (String word in words) {
        if (line.length + word.length <= firstColumnSymbolsCount &&
            isInProcess) {
          line += word;
          line += ' ';
          removedIndex = words.indexOf(word);
        } else {
          isInProcess = false;
          break;
        }
      }
      if (removedIndex != null) {
        if (removedIndex == 0)
          words.removeAt(0);
        else
          words.removeRange(0, removedIndex);
      }
      removedIndex = null;

      if (words.isNotEmpty) {
        line += '\n';
      }
      result.add(line);
      line = '';
    }
    return result.join() + '\n';
  }

  String getPriceLine(String name, String price) {
    final int numOfSpaces = allSymbolsCount - (name.length + price.length);
    String spaces = '';
    for (int i = 0; i < numOfSpaces; i++) {
      spaces += ' ';
    }
    return name + spaces + price + '\n';
  }
}
